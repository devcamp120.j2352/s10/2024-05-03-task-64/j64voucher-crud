package com.devcamp.j64crudapi.service;

import java.util.ArrayList;

import com.devcamp.j64crudapi.model.CVoucher;
import com.devcamp.j64crudapi.repository.IVoucherRepository;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class CVoucherService {
    @Autowired
	IVoucherRepository pVoucherRepository;
    public ArrayList<CVoucher> getVoucherList() {
        ArrayList<CVoucher> listCVoucher = new ArrayList<>();
        pVoucherRepository.findAll().forEach(listCVoucher::add);
        return listCVoucher;
    }
}